#!/usr/bin/env python

# This is used to do in-year validation (can be used for model validation)
import pandas as pd
import numpy as np
from sklearn import preprocessing
from sklearn.model_selection import StratifiedKFold
from sklearn.neighbors import KNeighborsClassifier
from sklearn import svm
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix

def main():
    # Set seed for reproducibility
    np.random.seed(0)

    for i in range(1, 25):
      # Reading raw data
      dataset = pd.read_csv('2016-week'+str(i)+'.csv',header=0)
      #dataset = pd.read_csv('week4.csv',header=0)
      dataset_values = dataset.drop('student_number',axis=1).drop('passed',axis=1)

      # First we massage a bit the input: normalisation (mostly for attendance)
      dataset_normal = preprocessing.MinMaxScaler().fit_transform(dataset_values)
      #dataset_normal = dataset_values.values
   
      # CONTINUE HERE!

      # Now we build x and y values:
      y_true = dataset['passed']
      X = dataset_normal

      # Our classifiers
      clf = KNeighborsClassifier(n_neighbors=5)
      clf2 = svm.SVC()
      clf3 = RandomForestClassifier()

      # StratifiedKFold: "each set contains approximately the same percentage of samples of each target class as the complete set."
      NSPLITS=3
      cm_knn = [[0,0],[0,0]]
      cm_svm = [[0,0],[0,0]]
      cm_rf = [[0,0],[0,0]]
      skf = StratifiedKFold(n_splits=NSPLITS)
      for train_index, test_index in skf.split(X,y_true):
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y_true[train_index], y_true[test_index]
        clf.fit(X_train,y_train)
        y_pred = clf.predict(X_test)
        cnf_matrix = confusion_matrix(y_test, y_pred)
        cm_knn += cnf_matrix

        clf2.fit(X_train,y_train)
        y_pred = clf2.predict(X_test)
        cnf_matrix = confusion_matrix(y_test, y_pred)
        cm_svm += cnf_matrix
        clf3.fit(X_train,y_train)
        y_pred = clf3.predict(X_test)
        cnf_matrix = confusion_matrix(y_test, y_pred)
        cm_rf += cnf_matrix
      print " ******** Results for week "+str(i)+" **********"
      print "KNN RESULTS: "
      print cm_knn/NSPLITS
      print "\nSVM RESULTS: "
      print cm_svm/NSPLITS
      print "\nRandom Forest RESULTS: "
      print cm_rf/NSPLITS
      print "\n\n"
    
if __name__ == '__main__':
    main()
