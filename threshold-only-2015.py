import pymysql.cursors
import pymysql
import csv

# This is used to extract threshold SOBs only

MAX_THRESHOLD_SOBS=34


def big_query(weeknumber,maxdate,connection):
  # First I get the list of SOBs
  with connection.cursor() as cursor:
    sql = "SELECT  sob_id from sobs where sob_id <40"
    cursor.execute(sql)
    data = cursor.fetchall()
    # There must be another way for doing this but I'm too lazy to look for it
    sobs = map((lambda x:x['sob_id']),data)
    
    # Let's build the string for the big query. The result is a table with:
    # - student number (M00123456)
    # - one row for each SOB. The value is 0 if the SOB has been observed by
    #   the maxdate
    # - one value for attendance (just the sum of sessions attended up to
    #   the weeknumber
    # - Last column: 1 if the student has passed the year (i.e., if the number
    #   of observed SOBs is = to the number of threshold SOB), otherwise 0

    big_query_sql = "select student_number, "
    
    # Now let's build the columns for SOBs.
    sobs_sql = map((lambda x: "(exists(select 1 from sob_observations as so where so.sob_id ="+str(x)+" and so.student_id=students.student_id and observed_on < '"+maxdate+"')) as sob"+str(x)),sobs)

    # And let's add them to the big_query_sql:
    big_query_sql += ','.join(sobs_sql)

    # Now let's add attendance:
    big_query_sql += ", (select count(*) from attendance where attendance.studid=students.student_number and attendance.week < "+str(weeknumber)+") as attendance"

    # And let's conclude by adding if student passed (checking sum of
    # threshold SOBs = MAX_THRESHOLD_SOBS). 
    # I'm also removing demo student and part-time students
    # FIXME: checking sob_id < 100 for threshold only works from 2016/17
    #        To do this properly you should check level in table sobs.
    big_query_sql += ", (select count(*)="+str(MAX_THRESHOLD_SOBS)+" from sob_observations where sob_id < 100 and sob_observations.student_id=students.student_id) as passed from students where student_id != 134"
    cursor.execute(big_query_sql)
    data = cursor.fetchall()

    #print big_query_sql

    # Just writing to file (with column names extracted from cursor)
    with open("threshold-2015-week"+str(weeknumber)+".csv", "wb") as csv_file:
      csv_writer = csv.writer(csv_file)
      csv_writer.writerow([i[0] for i in cursor.description]) # write headers
      for row in data:
         fields = [row[fieldname] for fieldname in [i[0] for i in cursor.description]]
         csv_writer.writerow(fields)
      csv_file.close()

# "Main" part of code: just get the list of weeks and call big_query
connection = pymysql.connect(host='localhost',
                             user='USERNAME',
                             password='PASSWORD',
                             db='DB_NAME',
                             cursorclass=pymysql.cursors.DictCursor)

try:
    with connection.cursor() as cursor:
        sql = "SELECT  week_number, week_end from week"
        cursor.execute(sql)
        data = cursor.fetchall()
        for row in data: 
          big_query(int(row['week_number']),str(row['week_end']),connection) 
finally:
    connection.close()

