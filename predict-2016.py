#!/usr/bin/env python

# This is used to predict 2016 data training on 2015 data (it works on threshold only)

import pandas as pd
import numpy as np
from sklearn import preprocessing
from sklearn.model_selection import StratifiedKFold
from sklearn.neighbors import KNeighborsClassifier
from sklearn import svm
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix, classification_report

def main():
    # Set seed for reproducibility
    np.random.seed(0)

    for i in range(8, 9):
      # Reading raw data
      training_dataset = pd.read_csv('data/2015-thresholdonly-week'+str(i)+'.csv',header=0)
      training_values = training_dataset.drop('student_number',axis=1).drop('passed',axis=1)


      prediction_dataset = pd.read_csv('data/2016-thresholdonly-week'+str(i)+'.csv',header=0)
      prediction_values = prediction_dataset.drop('student_number',axis=1).drop('passed',axis=1)
     
      # First we massage a bit the input: normalisation (mostly for attendance)
      training_normal = preprocessing.MinMaxScaler().fit_transform(training_values)
      prediction_normal = preprocessing.MinMaxScaler().fit_transform(prediction_values)

      student_numbers = prediction_dataset['student_number']
   
      # CONTINUE HERE!

      # Now we build x and y values:
      y_true = prediction_dataset['passed']
      y_training = training_dataset['passed']
      X = training_normal

      # Our classifiers
      clf = KNeighborsClassifier(n_neighbors=3)
      clf2 = svm.SVC(C=100,gamma=0.001)
      clf3 = RandomForestClassifier()
      clf4 = LogisticRegression()

      modelknn = clf.fit(X,y_training)
      modelsvm = clf2.fit(X,y_training)
      modelrf = clf3.fit(X,y_training)
      modellr = clf4.fit(X,y_training)

      y_pred1 = modelknn.predict(prediction_normal)
      y_pred2 = modelsvm.predict(prediction_normal)
      y_pred3 = modelrf.predict(prediction_normal)
      y_pred4 = modellr.predict(prediction_normal)

      cnf_matrix = confusion_matrix(y_true, y_pred1)
      print " ******** Results for week "+str(i)+" **********"
      print "KNN RESULTS: "
      #print cnf_matrix
      print classification_report(y_true, y_pred1)

      cnf_matrix = confusion_matrix(y_true, y_pred2)
      print "\nSVM RESULTS: "
      #print cnf_matrix
      print classification_report(y_true, y_pred2)

      cnf_matrix = confusion_matrix(y_true, y_pred3)
      print "\nRandom Forest RESULTS: "
      #print cnf_matrix
      print classification_report(y_true, y_pred3)

      cnf_matrix = confusion_matrix(y_true, y_pred4)
      print "\nLinear Regression RESULTS: "
      #print cnf_matrix
      print classification_report(y_true, y_pred4)
      print "\n\n"

      results_df = pd.DataFrame(data={'prediction':y_pred2})
      real_df = pd.DataFrame(data={'passed':y_true})
      joined = pd.DataFrame(student_numbers).join(results_df).join(real_df)
      joined.to_csv("svm-week8-predictions.csv", index=False)

      results_df = pd.DataFrame(data={'prediction':y_pred1})
      real_df = pd.DataFrame(data={'passed':y_true})
      joined = pd.DataFrame(student_numbers).join(results_df).join(real_df)
      joined.to_csv("knn-week8-predictions.csv", index=False)
 
if __name__ == '__main__':
    main()
