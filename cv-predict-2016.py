#!/usr/bin/env python

# This is used to predict 2016 data training on 2015 data (it works on threshold only)

import pandas as pd
import numpy as np
from sklearn import preprocessing
from sklearn.neighbors import KNeighborsClassifier
from sklearn import svm
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.metrics import classification_report
from sklearn.model_selection import GridSearchCV

def main():
    # Set seed for reproducibility
    np.random.seed(0)

    for i in range(16, 17):
      # Reading raw data
      training_dataset = pd.read_csv('data/2015-thresholdonly-week'+str(i)+'.csv',header=0)
      training_values = training_dataset.drop('student_number',axis=1).drop('passed',axis=1)


      prediction_dataset = pd.read_csv('data/2016-thresholdonly-week'+str(i)+'.csv',header=0)
      prediction_values = prediction_dataset.drop('student_number',axis=1).drop('passed',axis=1)
     
      # First we massage a bit the input: normalisation (mostly for attendance)
      training_normal = preprocessing.MinMaxScaler().fit_transform(training_values)
      prediction_normal = preprocessing.MinMaxScaler().fit_transform(prediction_values)

      student_numbers = prediction_dataset['student_number']
   
      # CONTINUE HERE!

      # Now we build x and y values:
      y_true = prediction_dataset['passed']
      y_training = training_dataset['passed']
      X = training_normal

      tuned_parameters_svm = [{'kernel': ['rbf'], 'gamma': [1e-3, 1e-4],
                   'C': [1, 10, 100, 1000]},
                   {'kernel': ['linear'], 'C': [1, 10, 100, 1000]}]

      tuned_parameters_knn = [{'n_neighbors':[3,5,7,9,11],
                               'p':[1,2],
                               'weights':['uniform','distance']}]

      tuned_parameters_rf = [{'n_estimators': [5,8,10,12,15,18,20],
                              'max_depth': [5,8,10,50,100,1000]}]

      # Our classifiers
      clf = GridSearchCV(KNeighborsClassifier(n_neighbors=3),
                     tuned_parameters_knn, cv=5,scoring='roc_auc')
      clf2 = GridSearchCV(svm.SVC(C=1), tuned_parameters_svm, cv=5,
                     scoring='roc_auc')

      clf3 = GridSearchCV(RandomForestClassifier(), tuned_parameters_rf, cv=5,
                     scoring='roc_auc')
      clf4 = LogisticRegression()

      modelknn = clf.fit(X,y_training)
      print("Best parameters set found for KNN")
      print()
      print(clf.best_params_)
      print()
      print("Grid scores on development set:")
      print()
      means = clf.cv_results_['mean_test_score']
      stds = clf.cv_results_['std_test_score']
      for mean, std, params in zip(means, stds, clf.cv_results_['params']):
          print("%0.3f (+/-%0.03f) for %r"
                % (mean, std * 2, params))
      print()

      clf = KNeighborsClassifier(n_neighbors=11,p=1)
      clf.fit(X,y_training)

      modelsvm = clf2.fit(X,y_training)

      print " ******** Results for week "+str(i)+" **********"
      print("Best parameters set found for SVM")
      print()
      print(clf2.best_params_)
      print()
      print("Grid scores on development set:")
      print()
      means = clf2.cv_results_['mean_test_score']
      stds = clf2.cv_results_['std_test_score']
      for mean, std, params in zip(means, stds, clf2.cv_results_['params']):
          print("%0.3f (+/-%0.03f) for %r"
                % (mean, std * 2, params))
      print()


      modelrf = clf3.fit(X,y_training)

      print("Best parameters set found for Random Forest")
      print()
      print(clf3.best_params_)
      print()
      print("Grid scores on development set:")
      print()
      means = clf3.cv_results_['mean_test_score']
      stds = clf3.cv_results_['std_test_score']
      for mean, std, params in zip(means, stds, clf3.cv_results_['params']):
          print("%0.3f (+/-%0.03f) for %r"
                % (mean, std * 2, params))
      print()

      modellr = clf4.fit(X,y_training)

      y_pred1 = modelknn.predict(prediction_normal)
      y_pred2 = modelsvm.predict(prediction_normal)
      y_pred3 = modelrf.predict(prediction_normal)
      y_pred4 = modellr.predict(prediction_normal)

      cnf_matrix = confusion_matrix(y_true, y_pred1)
      print " ******** Results for week "+str(i)+" **********"
      print "KNN RESULTS: "
      print cnf_matrix
      print classification_report(y_true, y_pred1)

      cnf_matrix = confusion_matrix(y_true, y_pred2)
      print "\nSVM RESULTS: "
      print cnf_matrix
      print classification_report(y_true, y_pred2)

      cnf_matrix = confusion_matrix(y_true, y_pred3)
      print "\nRandom Forest RESULTS: "
      print cnf_matrix
      print classification_report(y_true, y_pred3)

      cnf_matrix = confusion_matrix(y_true, y_pred4)
      print "\nLinear Regression RESULTS: "
      print cnf_matrix
      print classification_report(y_true, y_pred4)
      print "\n\n"

      results_df = pd.DataFrame(data={'prediction':y_pred2})
      real_df = pd.DataFrame(data={'passed':y_true})
      joined = pd.DataFrame(student_numbers).join(results_df).join(real_df)
      joined.to_csv("svm-week16-predictions.csv", index=False)

      results_df = pd.DataFrame(data={'prediction':y_pred1})
      real_df = pd.DataFrame(data={'passed':y_true})
      joined = pd.DataFrame(student_numbers).join(results_df).join(real_df)
      joined.to_csv("knn-week16-predictions.csv", index=False)
 
if __name__ == '__main__':
    main()
